
<!-- MENU -->
	<div id="header-nav">
		<div id="menu_left">
			<a href="/troquer"><div class="logo""></div></a>
			<nav id="nav" class="categories">
				<ol class="nav-primary">
					<li class="level0 nav-1 first last" rel="collections"><a href="/troquer">Colecciones</a></li>
					<li class="level0 nav-1 first last" rel="categories"><a href="/troquer">Categorias</a></li>
					<li class="level0 nav-1 first last"><a href="closets.php">Clósets</a></li>
					<li class="level0 nav-1 first last"><a href="marca.php">Marcas</a></li>
					<li class="level0 nav-1 first last"><a href="vender.php">Vende</a></li>
				</ol>
			</nav>
		</div>
		<div id="menu_right">
			<nav class="actions">
				<img src="images/search.png">
				<div class="divisa">
					<p>MXN</p><span class="icon"></span>
				</div>
				<p class="call_login">Entra o Regístrate<p>
				<div class="kart"></div>
			</nav>
		</div>
		<div id="menu_extra">
			<h2 class="hoefler"><span>Hermès</span> — Kelly Taurillon 32</h2>
			<div class="extra_prices">
				<div class="price">
					<span>Precio Original</span>
					<p>$10,500.00</p>
				</div>
				<div class="price">
					<span>Precio Troquer</span>
					<p>$7,500.00</p>
				</div>
				<div class="price">
					<div class="button black">Talla G - Comprar Ahora</div>
				</div>
			</div>
		</div>

		<ul class="sub_menu" rel="collections">
			<a href="/troquer"><li>Colección 1</li></a>
			<a href="/troquer"><li>Colección 2</li></a>
			<a href="/troquer"><li>Colección 3</li></a>
			<a href="/troquer"><li>Colección 4</li></a>
		</ul>
		<ul class="sub_menu" rel="categories">
			<a href="categoria.php"><li>Ropa</li></a>
			<a href="categoria.php"><li>Bolsas</li></a>
			<a href="categoria.php"><li>Zapatos</li></a>
			<a href="categoria.php"><li>Accesorios</li></a>
		</ul>

		<div id="menu_button"></div>
		<div id="kart_mobile"></div>
		<ul id="menu_mobile">
			<div id="header_menu">
				<a href="/troquer"><div class="logo""></div></a>
				<div id="menu_close"></div>
			</div>
			<a href="/troquer"><li>Entra o Regístrate</li></a>
			<a href="/troquer"><li>Categorías</li></a>
			<a href="/troquer"><li>Lo último</li></a>
			<a href="closets.php"><li>Clósets</li></a>
			<a href="marca.php"><li>Marcas</li></a>
			<a href="/troquer"><li>Vende</li></a>
			<a href="/troquer"><li>Mi Bolsa</li></a>
		</ul>
	</div>
<!-- END MENU -->