<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable = yes">
	<title>Troquer - Vender</title>

	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/closet.css">
	<link rel="stylesheet" type="text/css" href="css/vender.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

	<?php include "includes/menu.php"; ?>
	<div id="vender">
		<h1 class="hoefler">Vende en Troquer</h1>
		<div class="line"></div>
		<iframe width="100%" height="100%" src="https://www.youtube.com/embed/v7ZPM_FZ-sw?showinfo=0&rel=0&iv_load_policy=3&controls=0&cc_load_policy=0" frameborder="0" allowfullscreen></iframe>
		
		<div class="vender_text">
			<div class="ilustration"></div>
			<div class="text">
				<h2 class="hoefler">¿Y qué significa vender Troquer?</h2>
				<p>Por deshacerte de lo que no necesitas...</p>
				<p>Una Troquera gana en promedio v$200,000 al año, tu clóset se vende aproximadamente 3 meses después de ser publicado… Aunque hay prendas que encuentran nueva dueña en días o incluso horas después de ponerse a la venta</p>
				<p>Por ejemplo, Bolsa Louis Vuitton, vendida en 2 horas</p>
				<p>Vestido Herve Leger. vendido en 8 horas</p>
			</div>
		</div>
		<p class="special">Te depositaremos tus ganancias mensualmente: si prefieres transferencia,<br>recibe el 70% de tus prendas, o hasta el 80% con Crédito Troquer.</p>
		<h2 class="hoefler">¿Y qué tienes que hacer tú?<br>¡Nada!</h2>
		<p>Deshazte de lo que ya no necesitas y nosotros nos encargamos de todo.</p>
		<p>Dato: Nuestra área de curaduría toma en cuenta varios factores<br>para determinar el precio de tus prendas:</p>
		<ul>
			<li>• Haber salido de la tienda</li>
			<li>• Marca</li>
			<li>• Año de compra y modelo</li>
			<li>• Estado de la prenda</li>
			<li>• IVA, comisión de forma de pago y cuota de limpieza</li>
		</ul>
		<div class="check">
			<p><span></span>Acepto que todas las prendas que suba son auténticas. He leído los <a href="">Términos y condiciones</a></p>
		</div>
		<div class="button">Empezar a vender</div>
	</div>

	<footer></footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="js/waypoint.js"></script>
	<script src="js/functions.js"></script>
	<script src="js/vender.js"></script>
</body>
</html>