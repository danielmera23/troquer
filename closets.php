<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable = yes">
	<title>Troquer - Clósets</title>

	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/closets.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

	<?php include "includes/menu.php"; ?>

	<div id="closets">
		<div class="title_closets" style="background:url(images/closets/banner.png); background-size:cover">
			<h1 class="hoefler">Clósets</h1>
		</div>

		<div class="search_closets">
			<input type="text" name="" placeholder="Buscar closet...">
			<ul>
				<li><a href="closet.php">Alexandra de la Mora</a></li>
				<li><a href="closet.php">Adriana Rodríguez</a></li>
				<li><a href="closet.php">Andy Benavides</a></li>
				<li><a href="closet.php">Brenda Diaz de la Vega</a></li>
				<li><a href="closet.php">Brenda Jaet</a></li>
				<li><a href="closet.php">IT Fashion Editor</a></li>
				<li><a href="closet.php">Alexandra de la Mora</a></li>
				<li><a href="closet.php">Adriana Rodríguez</a></li>
				<li><a href="closet.php">Andy Benavides</a></li>
				<li><a href="closet.php">Brenda Diaz de la Vega</a></li>
				<li><a href="closet.php">Brenda Jaet</a></li>
				<li><a href="closet.php">IT Fashion Editor</a></li>
				<li><a href="closet.php">Alexandra de la Mora</a></li>
				<li><a href="closet.php">Adriana Rodríguez</a></li>
				<li><a href="closet.php">Andy Benavides</a></li>
				<li><a href="closet.php">Brenda Diaz de la Vega</a></li>
				<li><a href="closet.php">Brenda Jaet</a></li>
				<li><a href="closet.php">IT Fashion Editor</a></li>
				<li><a href="closet.php">Alexandra de la Mora</a></li>
				<li><a href="closet.php">Adriana Rodríguez</a></li>
				<li><a href="closet.php">Andy Benavides</a></li>
				<li><a href="closet.php">Brenda Diaz de la Vega</a></li>
				<li><a href="closet.php">Brenda Jaet</a></li>
				<li><a href="closet.php">IT Fashion Editor</a></li>
			</ul>
		</div>
		<div class="result_closets">
			<h3>Recién llegados</h3>
			
			<!-- FOREACH CLOSET -->
			<div class="closet">
				<a href="closet.php"><img src="images/demos/closet1.png"></a>
				<a href="closet.php"><h2 class="hoefler">Martha Debayle</h2></a>
				<p>Sofisticación y atemporalidad definen las piezas del nuevo clóset de Martha. Una colección exclusiva que merece ser estrenada otra vez.</p>
				<ul>
					<li class="hoefler"><a href="/troquer">Chanel</a></li>
					<li class="hoefler"><a href="/troquer">Alexander McQueen</a></li>
					<li class="hoefler"><a href="/troquer">Ann Taylor</a></li>
					<li class="hoefler"><a href="/troquer">BCBG</a></li>
				</ul>
				<div>SEGUIR CLOSET</div>
			</div>
			<div class="closet">
				<a href="closet.php"><img src="images/demos/closet2.png"></a>
				<a href="closet.php"><h2 class="hoefler">Alexandra de la Mora</h2></a>
				<p>La música y los festivales inspiran en estilo Boho-Chic de la DJ y actriz. Encuentra grandes piezas para completar tus looks alternativos.</p>
				<ul>
					<li class="hoefler"><a href="/troquer">Chanel</a></li>
					<li class="hoefler"><a href="/troquer">Alexander McQueen</a></li>
					<li class="hoefler"><a href="/troquer">Ann Taylor</a></li>
					<li class="hoefler"><a href="/troquer">BCBG</a></li>
				</ul>
				<div>SEGUIR CLOSET</div>
			</div>
			<div class="closet">
				<a href="closet.php"><img src="images/demos/closet3.png"></a>
				<a href="closet.php"><h2 class="hoefler">Adriana Rodríguez</h2></a>
				<p>Una combinación entre las últimas tendencias y prendas clásicas que no puede faltar en tu guardarropa.</p>
				<ul>
					<li class="hoefler"><a href="/troquer">Chanel</a></li>
					<li class="hoefler"><a href="/troquer">Alexander McQueen</a></li>
					<li class="hoefler"><a href="/troquer">Ann Taylor</a></li>
					<li class="hoefler"><a href="/troquer">BCBG</a></li>
				</ul>
				<div>SEGUIR CLOSET</div>
			</div>
		</div>
	</div>

	<footer></footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="js/waypoint.js"></script>
	<script src="js/functions.js"></script>
</body>
</html>