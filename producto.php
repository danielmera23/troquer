<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable = yes">
	<title>Troquer - Product</title>

	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/product.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	
</head>
<body>

	<?php include "includes/menu.php"; ?>

	<div id="product">
		<div id="product_left">
			<img id="image_product" src="images/product.png">
		</div>
		<div id="product_right">
			<div id="details">
				<div class="breadcrumbs">
					<div class="breadcrumb"><a href="/troquer">Inicio</a><span></span><a href="/troquer">Bolsas</a><span></span><a href="/troquer">Adriana Rodríguez</a></div>
					<div class="send_to"><span></span><p>Enviar a una amiga</p></div>
				</div>

				<h2 class="hoefler">Hermès</h2>
				<h1 class="hoefler">Kelly Taurillon 32</h1>
				<h3>Del clóset de Adriana Rodríguez</h3>
				
				<div id="prices">
					<div class="price">
						<p>Precio en tienda</p>
						<h4 class="not_price">$10,500.00</h4>
					</div>
					<div class="price">
						<p class="sale">SALE</p>
						<h4>$7,500.00</h4>
					</div>
					<div id="product_buttons" class="buttons">
						<div class="button">Agregar a la bolsa</div>
						<div class="button black">Talla G - Comprar Ahora</div>
						<br>
						<a href="/troquer">Guía de tallas</a>
					</div>
					<p class="scroll">Más información</p><span class="icon"></span>
				</div>
			</div>
		</div>
	</div>

	<div id="product_section">
		<div class="images">
			<img src="images/demo1.png">
			<img src="images/demo2.png">
			<img src="images/demo3.png">
		</div>
		<p>Icónica Kelly de 28 cm de ancho en piel de becerro, color negro</p>
		<p>Herrajes en color oro. Incluye su par de llaves y candado.</p>
		<p>Con un bolsillo interno con cremallera y dos bolsillos sin cierre.</p>
		<p>Se recomienda un buen mantenimiento para conservar su estado</p>
	</div>

	<div id="extras">
		<div id="extra_left">
			<img id="image_product2" src="images/product2.png">
		</div>
		<div id="extra_right">
			<div class="extra_text">
				<a href="/troquer">Guía de Materiales</a>
				<h2 class="hoefler">Características</h2>
				<div class="squares">
					<div class="square">
						<div class="color"></div>
						<p>Color</p>
					</div>
					<div class="square center">
						<div class="year"><p>'15</p></div>
						<p>Año</p>
					</div>
					<div class="square">
						<div class="size"><p>G</p></div>
						<p>Color</p>
					</div>
				</div>

				<div class="extra">
					<h2>Material</h2>
					<h3>Cuero</h3>
				</div>
				<div class="extra">
					<h2>Estado</h2>
					<h3>Se estrenó un par de veces</h3>
				</div>
				<div class="extra">
					<h2>Perfecta para</h2>
					<h3>Cualquier día</h3>
				</div>

				<div class="medidas">
					<h2>Medidas</h2>
					<div class="medida">
						<div>49cm</div>
						<p>Ancho</p>
					</div>
					<div class="medida">
						<div>27cm</div>
						<p>Alto</p>
					</div>
					<div class="medida">
						<div>17cm</div>
						<p>Profundidad</p>
					</div>
					<div class="medida">
						<div>22cm</div>
						<p>Asa</p>
					</div>
				</div>

				<div class="items">
					<div class="item">
						<h2>Número de producto (sku)</h2>
						<p>0145 - 3202 - 4030 - 4925</p>
					</div>
					<div class="item">
						<h2>Tienes dudas? Llámanos</h2>
						<p>+52 55 6843 - 4442</p>
					</div>
				</div>

				<div class="shipping">
					<h2>Detalles de Envio</h2>
					<p>De 2 a 3 días*</p>
				</div>
			</div>
		</div>
	</div>

	<div id="related">
		<h4>VISTOS ANTERIORMENTE</h4>
		<div class="related_content">
			<div class="related">
				<h2 class="hoefler">Antonio Ferardi</h2>
				<p>G</p>
				<img src="images/related.png">
				<h3><span>$8,600</span> $3,500</h3>
			</div>
			<div class="related">
				<h2 class="hoefler">Burberry</h2>
				<p>M</p>
				<img src="images/related2.png">
				<h3><span>$12,900</span> $8,000</h3>
			</div>
			<div class="related">
				<h2 class="hoefler">Avrone</h2>
				<p>S</p>
				<img src="images/related3.png" rel="images/related4.png">
				<h3><span>$7,500</span> $4,000</h3>
			</div>
		</div>
	</div>

	<footer></footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="js/waypoint.js"></script>
	<script src="js/functions.js"></script>
</body>
</html>