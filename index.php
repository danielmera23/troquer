<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable = yes">
	<title>Troquer - Home</title>

	<link rel="stylesheet" type="text/css" href="css/slick.css">
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/home.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	
</head>
<body>

	<?php include "includes/menu.php"; ?>

	<a href="/troquer" id="custom_banner" style="background:url(images/demos/bg-banner.png)">
		<h2 class="hoefler">¡Felicidades, mamá!</h2>
		<p>Prendas únicas para mamá, a precios irrestibles</p>
	</a>

	<div id="slider_home">
		<!-- FOREACH BANNER-->
		<a href="closet.php" class="slider_banner">
			<img src="images/demos/demo_slider.png">
			<div class="slider_text">
				<h2 class="hoefler"><span>El clóset de</span>Adriana Rodríguez</h2>
				<p>Prendas que nunca han sido portadas, esperando a que las estrenes</p>
			</div>
		</a>
		<a href="closet.php" class="slider_banner">
			<img src="images/demos/demo_slider.png">
			<div class="slider_text">
				<h2 class="hoefler"><span>El clóset de</span>Adriana Rodríguez</h2>
				<p>Prendas que nunca han sido portadas, esperando a que las estrenes</p>
			</div>
		</a>
		<a href="closet.php" class="slider_banner">
			<img src="images/demos/demo_slider.png">
			<div class="slider_text">
				<h2 class="hoefler"><span>El clóset de</span>Adriana Rodríguez</h2>
				<p>Prendas que nunca han sido portadas, esperando a que las estrenes</p>
			</div>
		</a>
	</div>

	<a href="closet.php" id="banner_closet">
		<div class="banner_text">
			<h2 class="hoefler"><span>El clóset de</span> Martha Debayle</h2>
			<p>Una colección exclusiva que merece ser estrenada otra vez.</p>
		</div>
		<img src="images/demos/demo_closet.png">
	</a>

	<div id="featured">
		<p class="section_title">COLECCIONES CURADAS POR NUESTRO EQUIPO</p>
		<!-- FOREACH COLLECTION -->
		<a href="/troquer" class="featured" style="background:url(images/demos/demo_collection.png)">
			<h2 class="hoefler">Listo para estrenar</h2>
			<div class="line"></div>
			<p>Prendas aún con etiqueta, listas para que las estrenes</p>
		</a>
		<a href="/troquer" class="featured" style="background:url(images/demos/demo_collection.png)">
			<h2 class="hoefler">Cheap n' cool</h2>
			<div class="line"></div>
			<p>Piezas con onda, a precios increibles</p>
		</a>
		<a href="/troquer" class="featured" style="background:url(images/demos/demo_collection.png)">
			<h2 class="hoefler">Bolsas más buscadas</h2>
			<div class="line"></div>
			<p>Completa tu look con las piezas deluxe más anheladas</p>
		</a>
	</div>

	<div id="top_search">
		<p class="section_title">LO MÁS BUSCADO</p>
		<!-- FOREACH SEARCH -->
		<a href="/troquer" class="hoefler">Chanel</a>
		<a href="/troquer" class="hoefler">Alexander McQueen</a>
		<a href="/troquer" class="hoefler">Ann Taylor</a>
		<a href="/troquer" class="hoefler">BCBG</a>
		<a href="/troquer" class="hoefler">Burberry</a>
		<a href="/troquer" class="hoefler">Balenciaga</a>
		<a href="/troquer" class="hoefler">Ballin</a>
		<a href="/troquer" class="hoefler">Céline</a>
	</div>

	<div id="home_extras">
		<a id="sell" href="/troquer">
			<h2 class="hoefler">¿Hay algo en tu clóset que ya no usas?</h2>
			<p>Vende tu ropa, bolsas, zapatos y accesorios en Troquer</p>
		</a>

		<a class="home_extra" href="/troquer">
			<img src="images/banner_brand.png">
			<div class="text">
				<h2 class="hoefler">Tus marcas favoritas, <br>garantizadas</h2>
				<p>Nuestros curadores verifican cada prenda para<br>asegurarte que compras una pieza original.</p>
			</div>
		</a>

		<a class="home_extra" href="/troquer">
			<img src="images/banner_new.png">
			<div class="text">
				<h2 class="hoefler">Como nuevo</h2>
				<p>Al llegar a Troquer todas las piezas pasan<br>por un riguroso proceso de sanitización.</p>
			</div>	
		</a>
	</div>
	
	<div id="related">
		<p class="section_title">PIEZAS DESTACADAS</p>
		<div class="related_content">
			<!-- FOREACH REALTED-->
			<a href="producto.php" class="related">
				<h2 class="hoefler">Antonio Ferardi</h2>
				<p>G</p>
				<img src="images/related.png">
				<h3><span>$8,600</span> $3,500</h3>
			</a>
			<a href="producto.php" class="related">
				<h2 class="hoefler">Burberry</h2>
				<p>M</p>
				<img src="images/related2.png">
				<h3><span>$12,900</span> $8,000</h3>
			</a>
			<a href="producto.php" class="related">
				<h2 class="hoefler">Avrone</h2>
				<p>S</p>
				<img src="images/related3.png" rel="images/related4.png">
				<h3><span>$7,500</span> $4,000</h3>
			</a>
			<a href="producto.php" class="related">
				<h2 class="hoefler">Antonio Ferardi</h2>
				<p>G</p>
				<img src="images/related.png">
				<h3><span>$8,600</span> $3,500</h3>
			</a>
			<a href="producto.php" class="related">
				<h2 class="hoefler">Burberry</h2>
				<p>M</p>
				<img src="images/related2.png">
				<h3><span>$12,900</span> $8,000</h3>
			</a>
			<a href="producto.php" class="related">
				<h2 class="hoefler">Avrone</h2>
				<p>S</p>
				<img src="images/related3.png" rel="images/related4.png">
				<h3><span>$7,500</span> $4,000</h3>
			</a>
		</div>
	</div>
	
	<footer></footer>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/waypoint.js"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
	<script type="text/javascript" src="js/home.js"></script>
</body>
</html>