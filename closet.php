<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable = yes">
	<title>Troquer - Clóset</title>

	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/closet.css">
	<link rel="stylesheet" type="text/css" href="css/section.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

</head>
<body>

	<?php include "includes/menu.php"; ?>

	<div id="closet">
		<div class="cover_closet" style="background:url(images/demos/single_closet.png) center no-repeat; background-size:cover">
			<h1 class="hoefler">Martha Debayle</h1>
			<div class="line"></div>
			<p>Martha Debayle regresa a Troquer con un clóset renovado. Una gran oportunidad para agregar estilo y piezas atemporales a tu colección.</p>
			<div class="shares">
				<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<iframe src="https://www.facebook.com/plugins/share_button.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&layout=button_count&size=small&mobile_iframe=true&appId=146843335490482&width=144&height=20" width="144" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
			</div>
			<div class="follow">SEGUIR CLOSET</div>

			<div class="breadcrumb"><a href="/troquer">Inicio</a><span></span><a href="closets.php">Clósets</a><span></span><a href="/troquer">Martha Debayle</a></div>
		</div>

		<div class="section">
			<div class="filters">
				<ul>
					<li>Violeta<span></span></li>
					<li>Burrberry<span></span></li>
					<li>Alexander McQueen<span></span></li>
					<li>Violeta<span></span></li>
					<li>Burrberry<span></span></li>
					<li>Alexander McQueen<span></span></li>
					<li>Violeta<span></span></li>
					<li>Burrberry<span></span></li>
					<li>Alexander McQueen<span></span></li>
					<li>Violeta<span></span></li>
					<li>Burrberry<span></span></li>
					<li>Alexander McQueen<span></span></li>
				</ul>
				
				<h3>Filtrar por:</h3>
				
				<!-- FOR EACH FILTER -->
				<div class="filter active">
					<h2>Categorias<span></span></h2>
					<div class="filter_content">
						<div class="line"></div>
						<div class="option">
							<div class="check"></div>
							<p>Zapatos</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Ropa</p>
						</div>
						<div class="option">
							<div class="check"></div>
							<p>Bolsas</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Accesorios</p>
						</div>
					</div>
				</div>

				<div class="filter active">
					<h2>Marca<span></span></h2>
					<div class="filter_content">
						<div class="line"></div>
						<div class="option">
							<div class="check"></div>
							<p>Ash</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Alexander McQueen</p>
						</div>
						<div class="option">
							<div class="check"></div>
							<p>Ann Taylor</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Bette Sung </p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Blank</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Burberry</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Caché</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Chanel</p>
						</div>
					</div>
				</div>

				<div class="filter active">
					<h2>Talla<span></span></h2>
					<div class="filter_content">
						<div class="line"></div>
						<div class="option">
							<div class="check"></div>
							<p>0</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>2</p>
						</div>
						<div class="option">
							<div class="check"></div>
							<p>4</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>6</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>8</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>10</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>12</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>14</p>
						</div>
					</div>
				</div>

				<div class="filter">
					<h2>Material<span></span></h2>
					<div class="filter_content">
						<div class="line"></div>
						<div class="option">
							<div class="check"></div>
							<p>Ash</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Alexander McQueen</p>
						</div>
						<div class="option">
							<div class="check"></div>
							<p>Ann Taylor</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Bette Sung </p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Blank</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Burberry</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Caché</p>
						</div>
						<div class="option active">
							<div class="check"></div>
							<p>Chanel</p>
						</div>
					</div>
				</div>

				<div class="filter">
					<h2>Color<span></span></h2>
					<div class="filter_content">
						<div class="line"></div>
						<div class="color red">
							<div></div>
						</div>
						<div class="color orange">
							<div></div>
						</div>
						<div class="color yellow">
							<div></div>
						</div>
						<div class="color brown">
							<div></div>
						</div>
						<div class="color green">
							<div></div>
						</div>
						<div class="color blue">
							<div></div>
						</div>
						<div class="color purple">
							<div></div>
						</div>
						<div class="color black">
							<div></div>
						</div>
						<div class="color white">
							<div></div>
						</div>
						<div class="color red">
							<div></div>
						</div>
						<div class="color orange">
							<div></div>
						</div>
						<div class="color yellow">
							<div></div>
						</div>
						<div class="color brown">
							<div></div>
						</div>
						<div class="color green">
							<div></div>
						</div>
						<div class="color blue">
							<div></div>
						</div>
						<div class="color purple">
							<div></div>
						</div>
						<div class="color black">
							<div></div>
						</div>
						<div class="color white">
							<div></div>
						</div>
					</div>
				</div>
			</div>
			<div class="items">
				<div class="header_items">
					<p>Monstrando 12 de 30 resultados</p>
					<select>
						<option>Order por</option>
						<option>Más nuevos</option>
						<option>Precio</option>
						<option>Talla</option>
					</select>
				</div>
				
				<div class="related_content">

					<!-- FOREACH ITEM-->
					<a href="producto.php" class="related">
						<h2 class="hoefler">Antonio Ferardi</h2>
						<p>G</p>
						<img src="images/related.png">
						<h3><span>$8,600</span> $3,500</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Burberry</h2>
						<p>M</p>
						<img src="images/related2.png">
						<h3><span>$12,900</span> $8,000</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Avrone</h2>
						<p>S</p>
						<img src="images/related3.png" rel="images/related4.png">
						<h3><span>$7,500</span> $4,000</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Antonio Ferardi</h2>
						<p>G</p>
						<img src="images/related.png">
						<h3><span>$8,600</span> $3,500</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Burberry</h2>
						<p>M</p>
						<img src="images/related2.png">
						<h3><span>$12,900</span> $8,000</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Avrone</h2>
						<p>S</p>
						<img src="images/related3.png" rel="images/related4.png">
						<h3><span>$7,500</span> $4,000</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Antonio Ferardi</h2>
						<p>G</p>
						<img src="images/related.png">
						<h3><span>$8,600</span> $3,500</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Burberry</h2>
						<p>M</p>
						<img src="images/related2.png">
						<h3><span>$12,900</span> $8,000</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Avrone</h2>
						<p>S</p>
						<img src="images/related3.png" rel="images/related4.png">
						<h3><span>$7,500</span> $4,000</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Antonio Ferardi</h2>
						<p>G</p>
						<img src="images/related.png">
						<h3><span>$8,600</span> $3,500</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Burberry</h2>
						<p>M</p>
						<img src="images/related2.png">
						<h3><span>$12,900</span> $8,000</h3>
					</a>
					<a href="producto.php" class="related">
						<h2 class="hoefler">Avrone</h2>
						<p>S</p>
						<img src="images/related3.png" rel="images/related4.png">
						<h3><span>$7,500</span> $4,000</h3>
					</a>
				</div>
				
				<ul class="pagination">
					<a href="/troquer" class="prev"><span></span><p>ANTERIOR</p></a>
					<a href="/troquer" class="active number">1</a>
					<a href="/troquer" class="number">2</a>
					<a href="/troquer" class="number">3</a>
					<a href="/troquer" class="number">4</a>
					<a href="/troquer" class="number">5</a>
					<a href="/troquer">...</a>
					<a href="/troquer" class="number">14</a>
					<a href="/troquer" class="next"><p>SIGUIENTE</p><span></span></a>
				</ul>
			</div>
		</div>
	</div>

	<footer></footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	<script src="js/waypoint.js"></script>
	<script src="js/functions.js"></script>
	<script src="js/filters.js"></script>
</body>
</html>