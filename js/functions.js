(function( $ ) {
	$(document).ready(function(){

		// *** PRODUCT VIEW

			function resize(){
				var height_w = $(window).height() - 60;
				var marginTop = (height_w/2) /2;
				if( $('#product').length ){
					$('#product').css('height',height_w+'px');
					$('#image_product').css('margin-top',marginTop+'px');
					$('#image_product2').css('margin-top',( (814 - $('#image_product2').height())/2 )+'px'); 
				}
			}

			$(window).resize(function() {
	  			resize();
			});

			if( $('#product_buttons').length ){
				var waypoint = new Waypoint({
					element: document.getElementById('product_buttons'),
					handler: function(direction) {
						if( $(window).width() > 767 ){
							if(direction=='down'){
								$('#header-nav').addClass('product');
								$('#menu_extra').stop().fadeIn(1000);
							}
							else{
								$('#header-nav').removeClass('product');
								$('#menu_extra').stop().fadeOut(0);
							}
						}
					},
					offset: 60
				})
			}

			if( $(window).width() > 767 ){
				resize();
			}

		// *** MENU
			if( $('#menu_button').on('click',function(){
				$('#menu_mobile').animate({height:396});
			}))

			if( $('#menu_close').on('click',function(){
				$('#menu_mobile').animate({height:0});
			}))
			
			$('#menu_left li').hover( function() {
					var rel = $(this).attr('rel');
					$('#menu_left li').removeClass('active');
					$(this).addClass('active')
					$('.sub_menu').fadeOut(0);
					$('.sub_menu[rel="'+rel+'"]').stop().fadeIn(0);
				}, function() {
					$('#menu_left li').removeClass('active');
				}
			);
			
			$('.sub_menu').hover( function() {
					var rel = $(this).attr('rel');
					$('#menu_left li[rel="'+rel+'"]').addClass('active');
				}, function() {
					var rel = $(this).attr('rel');
					$('#menu_left li').removeClass('active');
					$('.sub_menu').fadeOut(0);

				}
			);

			$('.related').hover( function() {
					var rel = $('img', this).attr('rel');
					var src = $('img', this).attr('src');
					$('img',this).attr('src',rel).attr('rel',src);
				}, function() {
					var rel = $('img', this).attr('rel');
					var src = $('img', this).attr('src');
					$('img',this).attr('src',rel).attr('rel',src);
					$('#menu_left li').removeClass('active');
				}
			);

			
			


			

	})

})( jQuery );