(function( $ ) {
	$(document).ready(function(){

		// ANIMATION FILTERS
		$('.filter h2').on('click',function(){
			var parent = $(this).parent();
			if( $(this).parent().hasClass('active') ){
				$('.filter_content', parent).animate({height: "0"},500);
				parent.removeClass('active');
			}else{
				$('.filter_content', parent).animate({height: "214px"},500);
				parent.addClass('active');
			}
		})
			
	})
})( jQuery );