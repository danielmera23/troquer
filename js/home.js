(function( $ ) {
	$(document).ready(function(){
		
		$('#slider_home').slick({
		  dots: false,
		  infinite: true,
		  speed: 300,
		  slidesToShow: 1,
		  adaptiveHeight: true
		});

	})

})( jQuery );