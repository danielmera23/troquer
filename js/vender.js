(function( $ ) {
	$(document).ready(function(){

		function video_size(){
			var width_w = $(window).width();
			var height_v =width_w / 2; 
			$('#vender iframe').css('height',height_v+'px');
		}

		$( window ).resize(function() {
			video_size();
		});

		video_size();

		$('.check span').on('click',function(){
			if( $(this).hasClass('active') )
				$(this).removeClass('active');
			else
				$(this).addClass('active');
		})
			
	})
})( jQuery );